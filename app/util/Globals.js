Ext.define('MediaCapture.util.Globals', {
  singleton: true,
  alternateClassName: 'globalUtils',
  version: '1.0',
  config: {
    phppath: 'http://localhost/MC',
  },
  constructor: function (config) {
    this.initConfig(config);
  },

  startRecordCordova: function () {
    let opts = { limit: 1 }
    navigator.device.capture.captureAudio(globalUtils.captureSuccess, globalUtils.captureError, opts);
  },

  captureSuccess: function (mediaFiles) {
    var i, path, len, file;
    name = mediaFiles[0].name;
    fileURL = mediaFiles[0].fullPath;
    type = mediaFiles[0].type;
    size = mediaFiles[0].size;
    for (i = 0, len = mediaFiles.length; i < len; i += 1) {

      mediaFile = mediaFiles[i];

      file = new File(mediaFile.name, mediaFile.localURL, mediaFile.type,
        mediaFile.lastModifiedDate, mediaFile.size);
    }
    if (size > 80000) {
      document.getElementById('recordingInfo').textContent = "Rekaman anda terlalu panjang.. silahkan coba lagi!";
    }
    else {
      document.getElementById('recordingInfo').textContent = "Menyimpan rekaman suara...";
      // console.log(file)
      // console.log(fileURL)
      // upload process
      var reader = new FileReader();
      reader.onloadend = function () {
        var blob = new Blob([new Uint8Array(this.result)], { type: "audio/mpeg" });
        var uri = "https://mediacapture.000webhostapp.com/MC/upload.php";
        // var uri ="https://192.168.43.11/MC/upload.php";
        var fd = new FormData();
        fd.append("file", blob);
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
          if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById('recordingInfo').textContent = "Telah terUpload"; // Another callback here
          } else {
            document.getElementById('recordingInfo').textContent = xhr.responseText;
          }
        };
        xhr.open("POST", uri, true);
        xhr.send(fd);
      }
      reader.readAsArrayBuffer(file);
      // var options = new FileUploadOptions();
      // options.fileKey = "file";
      // var filename = "rekaman.m4a";
      // options.fileName = filename;
      // options.mimeType = "text/plain";
      // // var ft = new FileTransfer();
      // // ft.upload(fileURL, uri, globalUtils.uploadSuccess, globalUtils.uploadError, options);
      //   window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
      //     fs.root.getFile(fs.name, { create: true, exclusive: false }, function (fileEntry) {
      //       console.log(fs.name)  
      //       fileEntry.file(function (file) {
      //           console.log(file);
      //             var reader = new FileReader();
      //             reader.onloadend = function() {
      //               var data = new FormData();
      //                 // Create a blob based on the FileReader "result", which we asked to be retrieved as an ArrayBuffer
      //                 var blob = new Blob([new Uint8Array(this.result)], { type: "audio/mpeg" });
      //                 var oReq = new XMLHttpRequest();
      //                 oReq.open("POST", "https://mediacapture.000webhostapp.com/MC/upload.php", true);
      //                 data.append('file', blob );
      //                 oReq.onload = function (oEvent) {
      //                     // all done!
      //                     document.getElementById('recordingInfo').textContent = oReq.responseText
      //                 };
      //                 // Pass the blob in to XHR's send method
      //                 oReq.send(data);
      //             };
      //             // Read the file as an ArrayBuffer
      //             reader.readAsArrayBuffer(file);
      //         }, function (err) { console.error('error getting fileentry file!' + err); });
      //     }, function (err) { console.error('error getting file! ' + err); });
      // }, function (err) { console.error('error getting persistent fs! ' + err); });


    }
  },
  captureError: function (error) {
    document.getElementById('recordingInfo').textContent = "error code : " + error.code;
  },

  uploadSuccess: function (r) {
    if (r.response == "failed") {
      document.getElementById('recordingInfo').textContent = "Rekaman gagal disimpan...";
    }
    else {
      document.getElementById('recordingInfo').textContent = "Rekaman berhasil disimpan...";
    }
  },

  uploadError: function (error) {
    Ext.Msg.alert("An Error has occurred : Code = " + error.code);
  }
})